#!/bin/bash
set -e

if ! (( $1 >= 0 && $1 < 65000)) 2>/dev/null; then
    echo "You should provide valid port number in range 0 - 65000"
    exit 1
fi

nginx_bind="-p $1:80"
if [ $1 -eq 0 ] ; then
    nginx_bind="--network none"
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${DIR}/.."

cd frontend
npm run build
cd ..

docker volume create --name inythm-gunicorn
# This volume can be already created by Docker-Webserver, but just in case
docker volume create --name nginx

docker build --pull -f docker/Dockerfile-starlette -t inythm-starlette:latest .
docker build --pull -f docker/Dockerfile-nginx -t inythm-nginx:latest .

docker stop inythm-starlette || true
docker rm inythm-starlette || true
docker run --name=inythm-starlette --restart=always \
    -v inythm-gunicorn:/gunicorn \
    --env-file=${PWD}/backend/.env \
    -d inythm-starlette:latest

docker stop inythm-nginx || true
docker rm inythm-nginx || true
docker run --name=inythm-nginx --restart=always \
    -v inythm-gunicorn:/gunicorn \
    -v nginx:/nginx \
    ${nginx_bind} \
    -d inythm-nginx:latest
